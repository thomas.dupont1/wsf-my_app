export interface ListButton {
  text: string;
  color: string;
}
