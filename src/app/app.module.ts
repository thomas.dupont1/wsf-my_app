import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DetailComponent } from './operations/components/detail/detail.component';
import { AppBodyComponent } from './app-body/app-body.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './operations/components/home/home.component';
import { ListComponent } from './operations/components/list/list.component';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AuthComponent } from './operations/components/auth/auth.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddVegetableComponent } from './operations/components/add-vegetable/add-vegetable.component';

@NgModule({
  declarations: [
    AppComponent,
    DetailComponent,
    AppBodyComponent,
    HomeComponent,
    ListComponent,
    AuthComponent,
    AddVegetableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [ CookieService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
