import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './operations/components/detail/detail.component';
import { HomeComponent } from './operations/components/home/home.component';
import { ListComponent } from './operations/components/list/list.component';
import { AuthComponent } from './operations/components/auth/auth.component';
import { AuthGuard } from './operations/guards/auth.guard';

const routes: Routes = [
  { path: 'login', component: AuthComponent },
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'list', component: ListComponent, canActivate: [AuthGuard] },
  { path: 'detail/:name', component: DetailComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
