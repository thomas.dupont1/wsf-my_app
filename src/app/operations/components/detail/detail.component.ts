import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { OperationsService } from '../../services/operations.service';
import { DetailParams } from '../../../types/detail-params.type';
import { Vegetables } from '../../../enums/vegetables.enum';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  public total: number;
  public name: string;

  constructor(private operationsService: OperationsService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.name = (this.activatedRoute.snapshot.params as DetailParams).name;
    this.total = this.operationsService.sum((this.name === Vegetables.Carottes) ? [5, 1, 10] : [10, 10, 10]);
  }
}
