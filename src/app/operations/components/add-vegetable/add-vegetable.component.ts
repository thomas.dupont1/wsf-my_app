import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-vegetable',
  templateUrl: './add-vegetable.component.html',
  styleUrls: ['./add-vegetable.component.scss']
})
export class AddVegetableComponent implements OnInit {

  @Input() vegetablesInAdd: string[];
  public name = new FormControl('');

  constructor() { }

  ngOnInit() {
  }

  addVegetable() {
    this.vegetablesInAdd.push(this.name.value);
  }

}
