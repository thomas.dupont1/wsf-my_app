import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVegetableComponent } from './add-vegetable.component';

describe('AddVegetableComponent', () => {
  let component: AddVegetableComponent;
  let fixture: ComponentFixture<AddVegetableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVegetableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVegetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
