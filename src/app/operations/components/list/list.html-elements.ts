import { ListButton } from '../../../types/templates/list-button.types';

export const showListButtonActive: ListButton = {
  text: 'Masquer',
  color: 'GREEN'
};

export const showListButtonInactive: ListButton = {
  text: 'Afficher',
  color: 'BLUE'
};
