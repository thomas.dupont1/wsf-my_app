import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ListButton } from '../../../types/templates/list-button.types';
import { showListButtonActive, showListButtonInactive } from './list.html-elements';
import { OperationsService } from '../../services/operations.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  public cookieValue: string;
  public vegetables: string[];
  public title: string;
  public showList: boolean;
  public showListButton: ListButton;
  public error: string;

  constructor(
    private router: Router,
    private operationsService: OperationsService,
    private cookieService: CookieService
  ) {
    this.title = 'title';
    this.showList = false;
    this.showListButton = showListButtonInactive;
  }

  ngOnInit() {
    this.cookieValue = this.cookieService.get('cookie-name');
    this.operationsService.fetchVegetables().subscribe((r: string[]) => {
      this.vegetables = r;
    }, (error: string) => this.error = error);
  }

  public showListAction() {
    this.showList = !this.showList;
    this.showListButton = this.showList ? showListButtonActive : showListButtonInactive;
  }

  public goToItems(index: number): void {
    this.router.navigate([`detail/${this.vegetables[index]}`]);
  }
}
