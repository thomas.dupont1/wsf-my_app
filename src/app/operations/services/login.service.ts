import { Injectable } from '@angular/core';
import { LoginForm } from '../../types/inputs/auth-input.types';
import { environment } from '../../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

const BASIC_AUTH = 'basic_auth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private cookieService: CookieService) { }

  public login({ email, password }: LoginForm): boolean {
    // fetch current user
    const user = environment.fakeData.users.find(u => {
      return u.password === password && u.email === email;
    });

    if (!user) {
      throw new Error('bad_credentials');
    }

    // set current user into cookie
    this.cookieService.set(BASIC_AUTH, btoa(`${email}:${password}`), 60);

    return true;
  }

  public isLogged(): boolean {
    // check the cookie existence
    if (!this.cookieService.check(BASIC_AUTH)) {
      return false;
    }

    // fetch the cookie
    const token = this.cookieService.get(BASIC_AUTH);

    // decode and deserialize the content
    const [ email, password ] = atob(token).split(':');

    // fetch the user related to the content
    const user = environment.fakeData.users.find(u => {
      return u.password === password && u.email === email;
    });

    // cast the user to boolean
    return !!user;
  }

  public logout() {
    this.cookieService.delete(BASIC_AUTH);
  }
}
