import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { handleError } from '../../errors';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OperationsService {
  public test: string;

  constructor(private http: HttpClient) {
    this.test = 'hello';
  }

  public sum(numbers: Array<number>): number {
    return numbers.reduce((p: number, c: number) => (p + c));
  }

  public fetchVegetables(): Observable<object> {
    return this.http.get('http://localhost:3000/api/vegetables', {
      params: {
        name: 'test'
      }
    }).
    pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(handleError) // then handle the error
    );
  }
}
