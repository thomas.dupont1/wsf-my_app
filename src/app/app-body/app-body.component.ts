import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../operations/services/login.service';

@Component({
  selector: 'app-body',
  templateUrl: './app-body.component.html',
  styleUrls: ['./app-body.component.scss']
})
export class AppBodyComponent implements OnInit {
  @Input() public title: string;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {}

  public logout() {
    this.loginService.logout();
    this.router.navigate(['login']);
  }

}
