export const environment = {
  production: true,
  fakeData: {
    users: [
      {
        email: 'test-prod@test.com',
        password: 'password'
      }
    ]
  }
};
